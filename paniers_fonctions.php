<?php
/**
 * Fonction du plugin Paniers
 *
 * @plugin     Paniers
 * @copyright  depuis 2013
 * @author     Collectif SPIP
 * @licence    GNU/GPL
 * @package    SPIP\Panier\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

/**
 * Tester si un objet ce trouve dans le panier de l'auteur connecté.
 *
 * @param mixed $id_objet
 * @param mixed $objet
 * @access public
 * @return mixed
 */
function dans_panier($id_objet, $objet) {
	include_spip('action/editer_liens');
	include_spip('inc/session');
	$objet_panier = objet_trouver_liens(
		['paniers' => session_get('id_panier')],
		[$objet => $id_objet]
	);

	return (empty($objet_panier)) ? false : true;
}
