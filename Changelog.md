# Plugin Paniers : notes de versions

## v1.5.0

Ajout d'un formulaire `#FORMULAIRE_REMPLIR_PANIER` en alternative au bouton d'action `remplir_panier`.
Par défaut il fait la même chose : juste un bouton, pas de message de retour.
Mais optionnellement, il permet pas mal de choses en plus, notamment :

- Afficher une saisie « quantité ».
- Afficher un message de retour.
- Recharger un bloc en ajax à la fin des traitements.
- Utiliser un event jQuery à la fin des traitements, par ex. pour afficher une modale.

Voir les commentaires de `formulaires_remplir_panier_options_dist()` pour toutes les options possibles.

Plus tard, cela permettra l’ajout d’autres saisies : choix d’une déclinaison, d’une taille, etc.

Ce formulaire fonctionne avec ou sans le plugin Saisies.

### Exemples d’utilisations

**Recharger un bloc ajax**

Imaginons qu'on ait dans la page une inclusion affichant le nombre d’articles dans le panier, qu’on souhaite recharger chaque fois qu’un article est ajouté.
Il suffit d’utiliser l’option `ajaxreload` :

```html
#FORMULAIRE_REMPLIR_PANIER{patate,#ID_PATATE,#ARRAY{
	ajaxreload, minipanier,
}}
```

L’inclusion :

```html
<INCLURE{fond=inclure/minipanier, ajax=minipanier}>
```

Et son contenu :

```html
#CACHE{0}
#SET{quantite, #ARRAY}
<BOUCLE_minipanier(PANIERS_LIENS) {id_panier=#SESSION{id_panier}}>#SET{quantite, #GET{quantite}|push{#QUANTITE}}</BOUCLE_minipanier>
#SET{quantite, #GET{quantite}|array_sum}
<span class="minipanier">
	<:paniers:mon_panier:>
	[<em>\((#GET{quantite})\)</em>]
</span>
```

De plus, on peut afficher un message concernant l’article ajouté, les infos sont présentes dans l'env dans la clé `remplir_panier` :

```html
[(#ENV{remplir_panier}|oui)
	<div class="alert alert-success alert-dismissible fade show" role="alert">
		[(#ENV{remplir_panier/quantite}|singulier_ou_pluriel{
			paniers:info_1_objet_remplir_panier,
			paniers:info_nb_objets_remplir_panier,
			nb,
			#ARRAY{objet,#INFO_TITRE{#ENV{remplir_panier/objet},#ENV{remplir_panier/id_objet}}}
		})]
	</div>
]
```

**Callback jQuery**

Imaginons maintenant qu'on veuille afficher une modale après l’ajout d’un article au panier.
Il suffit de se brancher sur l’évènement `remplir_panier` :

```javascript
$("body").on( "remplir_panier", function(event, data) {
	// Code pour afficher une modale, recharger un bloc, etc.
});
```

**Afficher la saisie quantité**

La saisie pour la quantité est soit `<select`>, soit un `<input>` de type `number`. On bascule automatiquement de l’une à l’autre en fonction d’un seuil et de la quantité max (il est aussi possible de forcer le type de saisie).

```html
#FORMULAIRE_REMPLIR_PANIER{patate,#ID_PATATE,#ARRAY{
	afficher_quantite, oui,
	quantite_max,      10,
	quantite_seuil,    5,
}}
```