<?php
/**
 * Fonction du plugin Paniers
 *
 * @plugin     Paniers
 * @copyright  depuis 2013
 * @author     Collectif SPIP
 * @licence    GNU/GPL
 * @package    SPIP\Panier\Pipelines
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Supprimer tous les paniers en cours qui sont trop vieux
function paniers_optimiser_base_disparus($flux) {
	$nombre = 0;
	include_spip('inc/config');
	// On cherche la date depuis quand on a le droit d'avoir fait le panier
	$depuis_ephemere = (intval(lire_config('paniers/limite_ephemere', 0)) ?: 24);
	$depuis_ephemere    = date('Y-m-d H:i:s', time() - 3600 * $depuis_ephemere);
	$depuis_enregistres = (intval(lire_config('paniers/limite_enregistres', 0)) ?: 168);
	$depuis_enregistres = date('Y-m-d H:i:s', time() - 3600 * $depuis_enregistres);

	// Soit le panier est à un anonyme donc on prend la limite éphémère, soit le panier appartient à un auteur et on prend l'autre limite
	$paniers = sql_allfetsel(
		'id_panier',
		'spip_paniers',
		'statut = ' . sql_quote('encours') . ' and ((id_auteur=0 and date<' . sql_quote($depuis_ephemere) . ') or (id_auteur>0 and date<' . sql_quote($depuis_enregistres) . '))'
	);

	if (is_array($paniers)) {
		$paniers = array_column($paniers, 'id_panier');
	}

	// S'il y a bien des paniers à supprimer
	if ($paniers) {
		// Le in
		$in = sql_in('id_panier', $paniers);

		// On supprime d'abord les liens
		sql_delete(
			'spip_paniers_liens',
			$in
		);

		// Puis les paniers
		$nombre = intval(sql_delete(
			'spip_paniers',
			$in
		));
	}

	$flux['data'] += $nombre;

	return $flux;
}

// La CSS pour le panier
function paniers_insert_head_css($flux) {
	$css  = timestamp(find_in_path('css/paniers.css'));
	$flux .= "<link rel='stylesheet' type='text/css' media='all' href='$css' />\n";

	return $flux;
}

/**
 * Sur une transformation de commande en attente
 * on supprime le panier source si besoin
 *
 * @param $flux
 *
 * @return @flux
 */
function paniers_post_edition($flux) {

	// Si on est dans le cas d'une commande qui passe de attente/en cours=>paye/livre/erreur
	if (
		isset($flux['args']['table']) and $flux['args']['table'] == 'spip_commandes'
		and $id_commande = $flux['args']['id_objet']
		and $flux['args']['action'] == 'instituer'
		and isset($flux['data']['statut'])
		and !in_array($flux['data']['statut'], ['attente', 'encours'])
		and in_array($flux['args']['statut_ancien'], ['attente', 'encours'])
		and $commande = sql_fetsel('id_commande, source', 'spip_commandes', 'id_commande=' . intval($id_commande))
	) {
		if (preg_match(',^panier#(\d+)$,', $commande['source'], $m)) {
			$id_panier        = intval($m[1]);
			$supprimer_panier = charger_fonction('supprimer_panier', 'action/');
			$supprimer_panier($id_panier);

			// nettoyer une eventuelle double commande du meme panier
			sql_updateq('spip_commandes', ['source' => ''], 'source=' . sql_quote($commande['source']));
			#spip_log('suppression panier '.$id_panier,'paniers');
		}
	}

	return $flux;
}
