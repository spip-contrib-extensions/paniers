<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paniers?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'action_ajouter' => 'Añadir a la cesta',
	'action_ajouter_objet' => 'Agregar "@objet@" a la cesta', # RELIRE
	'action_supprimer' => 'Retirar de la cesta', # RELIRE

	// C
	'champ_quantite_label' => 'Cantidad',
	'configurer_titre' => 'Configurar el plugin cesta',

	// D
	'description_noisette' => 'Mostrar la minicesta(Número de artículos et precio total)',
	'dont_total_taxe' => 'Total impuestos incluidos',

	// E
	'erreur_remplir_panier_quantite_max' => 'Cantidad máxima: @max@', # RELIRE
	'erreur_remplir_panier_quantite_min' => 'Cantidad mínima: @min@', # RELIRE

	// I
	'info_1_objet_panier' => '1 artículo en tu cesta', # RELIRE
	'info_1_objet_remplir_panier' => 'Se ha añadido 1 artículo al carrito', # RELIRE
	'info_1_titre_remplir_panier' => 'Se ha añadido "@objet@" a la cesta', # RELIRE
	'info_aucun_objet_panier' => 'No hay artículos en su cesta', # RELIRE
	'info_nb_objets_panier' => '@nb@ artículos en su cesta', # RELIRE
	'info_nb_objets_remplir_panier' => '@nb@ se han añadido artículos al carrito', # RELIRE
	'info_nb_titre_remplir_panier' => '@nb@ × "@objet@" se han agregado al carrito', # RELIRE

	// L
	'limite_enregistres_label' => 'Visitantes registrados',
	'limite_ephemere_label' => 'Visitantes no-registrados',
	'limite_explication' => 'Introduzca la duración de las cestas (en horas) por tipo de visitantes',
	'limite_titre' => 'Duración de la cesta',

	// M
	'mon_panier' => 'Mi cesta',
	'mon_panier_modifier' => 'Modificar mi cesta',
	'mon_panier_voir' => 'Ver mi cesta',

	// N
	'noisette_label_afficher_lien_page_panier' => 'Mostrar un enlace a la página spip.php?page=panier?', # RELIRE
	'noisette_label_afficher_titre_noisette' => '¿Mostrar un título?', # RELIRE
	'noisette_label_niveau_titre' => 'Elige el nivel del título', # RELIRE
	'noisette_label_titre_noisette' => 'Título:', # RELIRE
	'noisette_label_titre_noisette_perso' => 'Si es un título Personalizado:', # RELIRE
	'noisette_titre_perso' => 'Título Personalizado', # RELIRE
	'nom_bouton_plugin' => 'Cestas',
	'nom_noisette' => 'Minicesta',

	// P
	'panier_description' => 'Descripción',
	'panier_erreur_quantites' => 'Las cantidades deben ser números integrales',
	'panier_montant' => 'Precio',
	'panier_prix_unitaire' => 'Precio por unidad',
	'panier_quantite' => 'Cantidad',
	'panier_quantite_ok' => 'La cantidades han sido cambiados',
	'panier_recalculer' => 'Re-calcular',
	'panier_taxes' => 'Impuestos',
	'panier_total_ht' => 'Total sin IVA',
	'panier_total_ttc' => 'Total',
	'panier_valider' => 'Validar mi cesta',
	'panier_vide' => 'La cesta está vacía',
	'paniers' => 'Cestas',
	'produit_un' => '1 producto',
	'produits_plusieurs' => '@nb@ productos',

	// S
	'supprimer_du_panier' => 'Supprimer du panier', # RELIRE

	// T
	'titre_mon_panier' => 'Mi cesta',
	'titre_panier' => 'Cesta',

	// V
	'vider_le_panier' => 'Vaciar la cesta',
	'voir_mon_panier' => 'Mostrar mi cesta'
);
