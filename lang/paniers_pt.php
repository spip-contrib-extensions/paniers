<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paniers?lang_cible=pt
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'action_ajouter' => 'Adicionar aos pedidos',
	'action_ajouter_objet' => 'Adicionar « @objet@ » ao cesto',
	'action_supprimer' => 'Remover do cesto',

	// B
	'bouton_quantite_moins' => 'Reduzir a quantidade',
	'bouton_quantite_plus' => 'Aumentar a quantidade',
	'bouton_supprimer' => 'Remover este item',

	// C
	'champ_quantite_label' => 'Quantidade',
	'configurer_titre' => 'Configuração do plugin Cestos',

	// D
	'description_noisette' => 'Mostra o mini-cesto (Número de artigos e preço total)',
	'dont_total_taxe' => 'Em que o total de impostos',

	// E
	'erreur_remplir_panier_quantite_max' => 'Quantidade máxima: @max@',
	'erreur_remplir_panier_quantite_min' => 'Quantidade mínima: @min@',

	// I
	'info_1_objet_panier' => '1  item no seu cesto',
	'info_1_objet_remplir_panier' => '1 item foi adicionado ao cesto',
	'info_1_titre_remplir_panier' => '« @objet@ » foi adicionado ao cesto',
	'info_aucun_objet_panier' => 'Nenhum item no seu cesto',
	'info_nb_objets_panier' => '@nb@ items no seu cesto',
	'info_nb_objets_remplir_panier' => '@nb@ items foram adicionados ao cesto',
	'info_nb_titre_remplir_panier' => '@nb@ × « @objet@ » foram adicionados ao cesto',

	// L
	'limite_enregistres_label' => 'Visitantes registados',
	'limite_ephemere_label' => 'Visitantes não registados',
	'limite_explication' => 'Introduzir a duração (em horas) dos cestos, de acordo com o tipo de visitantes',
	'limite_titre' => 'Tempo de vida útil dos cestos',

	// M
	'mon_panier' => 'Os meus pedidos',
	'mon_panier_modifier' => 'Modificar os pedidos',
	'mon_panier_voir' => 'Ver os meus pedidos',

	// N
	'noisette_label_afficher_lien_page_panier' => 'Mostrar um link para a página spip.php ?page=panier ?',
	'noisette_label_afficher_titre_noisette' => 'Exibir um título?',
	'noisette_label_niveau_titre' => 'Escolher o nível do título',
	'noisette_label_titre_noisette' => 'Título:',
	'noisette_label_titre_noisette_perso' => 'Se com título personalizado:',
	'noisette_titre_perso' => 'Título personalizado',
	'nom_bouton_plugin' => 'Cestos',
	'nom_noisette' => 'Mini cesto',

	// P
	'panier_description' => 'Descrição',
	'panier_erreur_quantites' => 'As quantidades devem ser indicadas em números inteiros',
	'panier_montant' => 'Montante',
	'panier_prix_unitaire' => 'Preço unitário',
	'panier_quantite' => 'Quantidade',
	'panier_quantite_ok' => 'As quantidades foram modificadas correctamente.',
	'panier_recalculer' => 'Recalcular',
	'panier_supprimer_ok' => '« @objet@ » foi retirado do cesto',
	'panier_taxes' => 'Em que impostos',
	'panier_total_ht' => 'Total sem IVA',
	'panier_total_ttc' => 'Total',
	'panier_valider' => 'Validar os meus pedidos',
	'panier_vide' => 'O cesto está vazio.',
	'paniers' => 'Pedidos',
	'produit_aucun' => 'Nenhum item',
	'produit_un' => '1 produto',
	'produits_plusieurs' => '@nb@ produtos',

	// S
	'supprimer_du_panier' => 'Remover do cesto',

	// T
	'titre_mon_panier' => 'Os meus pedidos',
	'titre_panier' => 'Pedidos',

	// V
	'vider_le_panier' => 'Remover todos os pedidos',
	'voir_mon_panier' => 'Ver os meus pedidos'
);
