<?php
/**
 * Fonction du plugin Paniers
 *
 * @plugin     Paniers
 * @copyright  depuis 2013
 * @author     Collectif SPIP
 * @licence    GNU/GPL
 * @package    SPIP\Panier\Administrations
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'installation et de mise à jour du plugin paniers.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
 **/
function paniers_upgrade($nom_meta_base_version, $version_cible) {

	$maj = [];
	include_spip('base/abstract_sql');
	include_spip('inc/config');
	// Première installation
	// création des tables + options de configuration
	$maj['create'] = [
		['maj_tables', ['spip_paniers', 'spip_paniers_liens']],
		['ecrire_config', 'paniers', [
			'limite_ephemere' => '24',
			'limite_enregistres' => '168']
		]
	];

	// ajout du champ reduction
	$maj['0.3.0'] = [
		['maj_tables', ['spip_paniers_liens']],
	];

	// ajout du champ rang
	$maj['0.4.0'] = [
		['maj_tables', ['spip_paniers_liens']],
	];
	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Fonction de désinstallation du plugin paniers.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
 **/
function paniers_vider_tables($nom_meta_base_version) {


	include_spip('base/abstract_sql');
	include_spip('inc/config');

	// On efface les tables du plugin
	sql_drop_table('spip_paniers');
	sql_drop_table('spip_paniers_liens');

	// On efface la version entregistrée
	effacer_meta($nom_meta_base_version);
	effacer_config('paniers');
}
