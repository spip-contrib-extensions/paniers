<?php
/**
 * Fonction du plugin Paniers
 *
 * @plugin     Paniers
 * @copyright  depuis 2013
 * @author     Collectif SPIP
 * @licence    GNU/GPL
 * @package    SPIP\Panier\Action
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/base');

/**
 * Créer une commande et remplir ses détails d'après le panier en cours du visiteur (ou d'après un panier donné).
 *
 * Pour créer une commande d'après le panier présent dans la session:
 *   #URL_ACTION_AUTEUR{commandes_paniers,'',#SELF}
 * Pour créer une commande d'après un panier précis :
 *   #URL_ACTION_AUTEUR{commandes_paniers,#ID_PANIER,#SELF}
 * Sans redirection explicite, la fonction redirige vers la page de la commande
 *
 * @param string $arg
 *     id_panier pour creer la commande et le detruire
 *     id_panier-1 pour creer la commande et le conserver
 * @return void
 * */
function action_commandes_paniers_dist($arg = null) {

	// Si $arg n'est pas donné directement, le récupérer via _POST ou _GET
	if (is_null($arg)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	$arg = explode('-', $arg);
	$id_panier = 0;
	if (count($arg)) {
		$id_panier = intval(array_shift($arg));
	}
	$keep = false;
	if (count($arg)) {
		$keep = intval(array_shift($arg));
	}


	// Sans paramètre, récupérer $id_panier dans la session du visiteur actuel
	if (!$id_panier) {
		include_spip('inc/paniers');
		$id_panier = paniers_id_panier_encours();
	}

	// Si aucun panier ne pas agir
	if (!$id_panier) {
		return;
	}

	if (!empty($GLOBALS['visiteur_session']['id_auteur'])) {
		$id_auteur = $GLOBALS['visiteur_session']['id_auteur'];
	}
	else {
		$id_auteur = sql_getfetsel('id_auteur', 'spip_paniers', 'id_panier=' . intval($id_panier));
	}


	include_spip('inc/commandes');
	include_spip('inc/config');
	// si une commande recente est encours (statut et dans la session de l'utilisateur), on la reutilise
	// plutot que de recreer N commandes pour un meme panier
	// (cas de l'utilisateur qui revient en arriere puis retourne a la commande)
	include_spip('inc/session');
	$w = [
		'statut=' . sql_quote('encours'),
		'date>' . sql_quote(date('Y-m-d H:i:s', strtotime('-' . lire_config('paniers/limite_ephemere', 24) . ' hour'))),
		'source=' . sql_quote("panier#$id_panier"),
		'id_auteur=' .intval($id_auteur)
	];
	if (!empty(session_get('id_commande'))) {
		$w[] = 'id_commande=' . intval(session_get('id_commande'));
	}
	$id_commande = sql_getfetsel('id_commande', 'spip_commandes', $w);

	// sinon on cree une commande "en cours"
	if (!$id_commande) {
		$id_commande = creer_commande_encours();
	}

	// et la remplir les details de la commande d'après le panier en session
	if ($id_commande) {
		panier2commande_remplir_commande($id_commande, $id_panier, false, !$keep);
	}

	// Sans redirection donnée, proposer une redirection par defaut vers la page de la commande créée
	if (is_null(_request('redirect'))) {
		$GLOBALS['redirect'] = generer_url_public('commande', 'id_commande=' . $id_commande, true);
	}
}

/**
 * Remplir une commande d'apres un panier
 *
 * @param  int $id_commande
 * @param  int $id_panier
 * @param  bool $append
 *   true pour ajouter brutalement le panier a la commande, false pour verifier que commande==panier en ajoutant/supprimant uniquement les details necessaires
 * @param  bool $supprimer_panier_apres
 *   true pour supprimer le panier après créationd de la commande, false pour le conserver
 */
function panier2commande_remplir_commande($id_commande, $id_panier, $append = true, $supprimer_panier_apres = true) {

	include_spip('action/editer_objet');
	include_spip('inc/filtres');
	include_spip('inc/paniers');

	// noter le panier source dans le champ source de la commande
	objet_modifier('commande', $id_commande, ['source' => "panier#$id_panier"]);

	// recopier le contenu du panier dans la commande
	// On récupère le contenu du panier
	$panier = sql_allfetsel(
		'*',
		'spip_paniers_liens',
		'id_panier = ' . intval($id_panier)
	);

	// Pour chaque élément du panier, on va remplir la commande
	// (ou verifier que la ligne est deja dans la commande)
	if ($panier and is_array($panier)) {
		$details = [];
		include_spip('spip_bonux_fonctions');
		$fonction_prix = charger_fonction('prix', 'inc/');
		$fonction_prix_ht = charger_fonction('ht', 'inc/prix');
		foreach ($panier as $emplette) {
			$prix_ht = $fonction_prix_ht($emplette['objet'], $emplette['id_objet'], 6);
			$prix = $fonction_prix($emplette['objet'], $emplette['id_objet'], 6);

			// On déclenche un pipeline pour pouvoir éditer le prix avant la création de la commande
			// Utile par exemple pour appliquer une réduction automatique lorsque la commande est crée
			$prix_pipeline = pipeline(
				'panier2commande_prix',
				[
					'args' => $emplette,
					'data' => [
						'prix' => $prix,
						'prix_ht' => $prix_ht,
						'prix_modifie' => ''
					]
				]
			);

			// On ne récupère que le prix_ht dans le pipeline
			$prix_ht = $prix_pipeline['prix_ht'];
			$prix = $prix_pipeline['prix'];

			if ($prix_ht > 0) {
				$taxe = round(($prix - $prix_ht) / $prix_ht, 6);
			} else { $taxe = 0;
			}

			$set = [
				'id_commande' => $id_commande,
				'objet' => $emplette['objet'],
				'id_objet' => $emplette['id_objet'],
				'descriptif' => generer_info_entite($emplette['id_objet'], $emplette['objet'], 'titre', '*'),
				'quantite' => $emplette['quantite'],
				'reduction' => $emplette['reduction'],
				'prix_unitaire_ht' => $prix_ht,
				'taxe' => $taxe,
				'statut' => 'attente'
			];
			$where = [];
			foreach ($set as $k => $w) {
				if (in_array($k, ['id_commande', 'objet', 'id_objet'])) {
					$where[] = "$k=" . sql_quote($w);
				}
			}
			// est-ce que cette ligne est deja la ?
			if ($append or ! $id_commandes_detail = sql_getfetsel('id_commandes_detail', 'spip_commandes_details', $where)) {
				// sinon création et renseignement du détail de la commande
				$id_commandes_detail = objet_inserer('commandes_detail');
			}
			if ($id_commandes_detail) {
				objet_modifier('commandes_detail', $id_commandes_detail, $set);
				$details[] = $id_commandes_detail;

				pipeline(
					'post_edition',
					[
						'args' => [
							'table'    => 'spip_commandes_details',
							'id_objet' => $id_commandes_detail,
							'action'   => 'remplir_commande_detail',
							'emplette' => $emplette,
						],
						'data' => array_merge($set, $prix_pipeline)
					]
				);
			}
		}
		if (!$append) {
			// supprimer les details qui n'ont rien a voir avec ce panier
			sql_delete('spip_commandes_details', 'id_commande=' . intval($id_commande) . ' AND ' . sql_in('id_commandes_detail', $details, 'NOT'));
		}

		// Envoyer aux plugins après édition pour verification eventuelle du contenu de la commande
		// et peuvent modifier le $supprimer_panier_apres
		// (par exemple si 2 produits sont incompatibles et ne peuvent être commandés en même temps, l'un des 2 reste dans le panier que l'on ne doit pas supprimer)
		$res = pipeline(
			'post_edition',
			[
				'args' => [
					'table' => 'spip_commandes',
					'id_objet' => $id_commande,
					'action' => 'remplir_commande',
					'id_panier' => intval($id_panier),
				],
				// on passe un array en data pour ne pas declencher des erreurs sur les pipelines post_edition
				// qui attendent en general une liste de valeurs modifiees dans le pipeline
				'data' => [
					'supprimer_panier' => $supprimer_panier_apres
				],
			]
		);
		$supprimer_panier_apres = (empty($res['supprimer_panier']) ? false : true);
	}

	// Supprimer le panier ?
	if ($supprimer_panier_apres) {
		$supprimer_panier = charger_fonction('supprimer_panier', 'action/');
		$supprimer_panier($id_panier);
	}
}
