<?php
/**
 * Fonction du plugin Paniers
 *
 * @plugin     Commandes de Paniers
 * @copyright  depuis 2013
 * @author     Collectif SPIP
 * @licence    GNU/GPL
 * @package    SPIP\Panier2commande\Action
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Options du formulaire de panier
 *
 * @param integer $id_panier
 *     Identifiant d'un panier, à défaut celui en session
 * @param array $options
 *     Tableau d'options :
 *     - autosubmit                : poster automatiquement le formulaire (cache le bouton recalculer)
 *     - afficher_boutons_quantite : ajoute des boutons + et - pour modifier les quantités
 *     - afficher_boutons_supprimer: ajoute des boutons pour supprimer un objet
 *     - bouton_vider_class        : classes optionnelles du bouton vider
 *     - bouton_recalculer_class   : classes optionnelles du bouton recalculer
 *     - bouton_quantite_class     : classes optionnelles des boutons pour modifier les quantités
 *     - bouton_supprimer_class    : classes optionnelles du bouton pour retirer un objet
 * @return array
 */
function formulaires_panier_options($id_panier = 0, $options = []) {

	$options_defaut = [
		'autosubmit'                 => true,
		'afficher_boutons_quantite'  => true,
		'afficher_boutons_supprimer' => true,
		'bouton_vider_class'         => 'btn btn-link',
		'bouton_recalculer_class'    => 'btn btn-primary',
		'bouton_quantite_class'      => 'btn btn-link',
		'bouton_supprimer_class'     => 'btn btn-link',
	];
	$options_merge = array_merge($options_defaut, $options);

	return $options_merge;
}


/**
 * Chargement du formulaire de remplissage d'un panier
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @param integer $id_panier
 *     Identifiant d'un panier, à défaut celui en session
 * @param array $options
 *     Tableau d'options, cf. formulaires_panier_options()
 * @return array
 *     Environnement du formulaire
 */
function formulaires_panier_charger($id_panier = 0, $options = []) {

	// On commence par chercher le panier du visiteur actuel s'il n'est pas fourni à l'appel du formulaire
	include_spip('inc/paniers');
	if (!$id_panier) {
		$id_panier = paniers_id_panier_encours();
	}

	$contexte = [
		'_id_panier' => $id_panier,
		'quantites' => []
	];

	$opts = formulaires_panier_options($id_panier, $options);
	$contexte = array_merge($contexte, $opts);

	return $contexte;
}


/**
 * Vérifications du formulaire de panier
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @param integer $id_panier
 *     Identifiant d'un panier, à défaut celui en session
 * @param array $options
 *     Tableau d'options, cf. formulaires_panier_options()
 * @return array
 *     Tableau des erreurs
 */
function formulaires_panier_verifier($id_panier = 0, $options = []) {
	$erreurs = [];

	if (!_request('vider')) {
		$quantites = _request('quantites');

		$paniers_arrondir_quantite = charger_fonction('paniers_arrondir_quantite', 'inc');

		if (is_array($quantites)) {
			foreach ($quantites as $objet => $objets_de_ce_type) {
				foreach ($objets_de_ce_type as $id_objet => $quantite) {
					if (
						!is_numeric($quantite)
						or $quantite != $paniers_arrondir_quantite($quantite, $objet, $id_objet)
						or (
							is_numeric($quantite)
							and $quantite < 0
						)
					) {
						$erreurs['message_erreur'] = _T('paniers:panier_erreur_quantites');
						$erreurs['quantites'][$objet][$id_objet] = 'erreur';
					}
				}
			}
		}
	}

	return $erreurs;
}


/**
 * Traitement du formulaire de remplissage d'un panier
 *
 * Traiter les champs postés
 *
 * @param integer $id_panier
 *     Identifiant d'un panier, à défaut celui en session
 * @param array $options
 *     Tableau d'options, cf. formulaires_panier_options()
 * @return array
 *     Retours des traitements
 */
function formulaires_panier_traiter($id_panier = 0, $options = []) {
	$retours = [];
	$message_ok = '';

	// On commence par chercher le panier du visiteur actuel s'il n'est pas fourni à l'appel du formulaire
	include_spip('inc/paniers');
	if (!$id_panier) {
		$id_panier = paniers_id_panier_encours();
		// $message_ok = _T('paniers:panier_vider_ok');
	}

	// Vider la panier
	if (_request('vider')) {
		$supprimer_panier = charger_fonction('supprimer_panier', 'action');
		$supprimer_panier($id_panier);
	// Recalculer les quantités
	} else {
		$quantites = _request('quantites');
		$ok = 0;

		if (is_array($quantites)) {
			$paniers_arrondir_quantite = charger_fonction('paniers_arrondir_quantite', 'inc');

			foreach ($quantites as $objet => $objets_de_ce_type) {
				foreach ($objets_de_ce_type as $id_objet => $quantite) {
					$quantite = $paniers_arrondir_quantite($quantite, $objet, $id_objet);
					// Si la quantite est 0, on supprime du panier
					if ($quantite <= 0) {
						$delete = sql_delete(
							'spip_paniers_liens',
							'id_panier = ' . intval($id_panier) . ' and objet = ' . sql_quote($objet) . ' and id_objet = ' . intval($id_objet)
						);
						$ok += ($delete !== false ? 1 : 0);
						if ($ok) {
							include_spip('inc/filtres');
							$message_ok = _T('paniers:panier_supprimer_ok', ['objet' => generer_info_entite($id_objet, $objet, 'titre')]);
						}
					// Sinon on met à jour
					} else {
						if (
							$quantite != sql_getfetsel(
								'quantite',
								'spip_paniers_liens',
								'id_panier = ' . intval($id_panier) . ' and objet = ' . sql_quote($objet) . ' and id_objet = ' . intval($id_objet)
							)
						) {
							$update = sql_updateq(
								'spip_paniers_liens',
								['quantite' => $quantite],
								'id_panier = ' . intval($id_panier) . ' and objet = ' . sql_quote($objet) . ' and id_objet = ' . intval($id_objet)
							);
							$ok += ($update === true ? 1 : 0);
							if ($ok) {
								$message_ok = _T('paniers:panier_quantite_ok');
							}
						}
					}
					$args = [
						'id_panier' => $id_panier,
						'objet'    => $objet,
						'id_objet' => $id_objet,
						'quantite' => $quantite
					];
					pipeline('maj_panier', [ 'args' => $args ]);
				}
			}
		}
	}

	// Mais dans tous les cas on met la date du panier à jour
	sql_updateq(
		'spip_paniers',
		['date' => date('Y-m-d H:i:s')],
		'id_panier = ' . intval($id_panier)
	);

	if ($message_ok) {
		$retours['message_ok'] = $message_ok;
	}

	return $retours;
}
