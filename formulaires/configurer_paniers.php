<?php
/**
 * Fonction du plugin Paniers
 *
 * @plugin     Commandes de Paniers
 * @copyright  depuis 2013
 * @author     Collectif SPIP
 * @licence    GNU/GPL
 * @package    SPIP\Panier2commande\Action
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) { return;
}

function formulaires_configurer_paniers_saisies_dist() {
	include_spip('inc/config');
	$config = lire_config('paniers');

	return [
		[
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'fieldset_parametres',
				'label' => _T('paniers:limite_titre')
			],
			'saisies' => [
				[
					'saisie' => 'explication',
					'options' => [
						'nom' => 'exp1',
						'texte' => _T('paniers:limite_explication')
					]
				],
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'limite_ephemere',
						'label' => _T('paniers:limite_ephemere_label'),
						'defaut' => $config['limite_ephemere'],
						'conteneur_class' => 'long_label'
					]
				],
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'limite_enregistres',
						'label' => _T('paniers:limite_enregistres_label'),
						'defaut' => $config['limite_enregistres'],
						'conteneur_class' => 'long_label'
					]
				]
			]
		]
	];
}
