<?php
/**
 * Fonction du plugin Paniers
 *
 * @plugin     Paniers
 * @copyright  depuis 2013
 * @author     Collectif SPIP
 * @licence    GNU/GPL
 * @package    SPIP\Panier\Formulaires
 */

/**
 * Gestion du formulaire de remplissage d'un panier
 *
 * @note
 * Ce formulaire vient en complément du bouton d'action `remplir_panier`,
 * il propose des fonctionnalités supplémentaires.
 *
 * Par défaut ça fait la même chose, mais on peut optionnellement :
 * - Afficher une saisie « quantité ».
 * - Afficher un message de retour.
 * - Recharger un bloc donné en ajax.
 *   Les infos sur l'objet ajouté au panier sont transmises dans la clé `remplir_panier`.
 * - Utiliser un event jQuery pour faire des choses après, par ex. afficher une modale.
 *   Exemple :
 *   $("body").on( "remplir_panier", function(event, data) {
 *     // Recharger un bloc, afficher une modale, etc.
 *   });
 *
 * UX de la saisie quantité :
 * On choisit automatiquement un <select> ou un <input> selon ces règles :
 * - s'il y a une quantité max inférieure au seuil (10) : <select>
 * - s'il n'y a pas de quantité max ou qu'elle est supérieure au seuil : <input>
 *
 * @example
 * ````
 * #FORMULAIRE_REMPLIR_PANIER{patate, 10, #ARRAY{
 *   choisir_quantite,  oui,
 *   quantite_max,      3,
 *   ajaxreload,        minipanier,
 * }}
 * ````
 *
 * @plugin     Paniers
 * @copyright  2020
 * @author     Ldd
 * @licence    GNU/GPL
 * @package    SPIP\Paniers\Formulaires
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Options par défaut du formulaire de remplissage de panier
 *
 * @param array $options
 *     Tableau d'options :
 *     - choisir_quantite (bool)  : pour afficher ou pas l'input, defaut = false
 *     - quantite_min (int)       : quantité minimale, défaut = 1
 *     - quantite_max (int)       : quantité maximale, defaut = null
 *     - quantite_saisie (string) : type de saisie (auto | input | selection), défaut = auto
 *     - quantite_seuil (int)     : seuil pour basculer entre un <select> et un <input>, défaut = 10
 *     - ajaxreload               : identifiant d'un bloc ajax à recharger.
 *                                  l'env reçoit `objet` + `id_objet` + `quantite` dans la clé `remplir_panier`
 *     - submit_texte             : texte du bouton
 *     - submit_class             : classes ajoutées au bouton
 *     - submit_title             : attribut title du bouton
 * @return array
 *     Options par défaut
 */
function formulaires_remplir_panier_options_dist(
	$objet,
	$id_objet,
	$options = [],
	$retour = ''
) {
	static $options_static;
	$hash = md5(json_encode(func_get_args()));
	if (isset($options_static[$hash])) {
		return $options_static[$hash];
	}

	// Compat ancien nom
	if (isset($options['afficher_quantite'])) {
		$options['choisir_quantite'] = $options['afficher_quantite'];
	}

	include_spip('inc/filtres'); // pour generer_info_entite()
	include_spip('inc/texte'); // nécessaire à generer_info_entite() mais pas tout le temps chargé par elle (?)

	$options_defaut = [
		'afficher_message_ok' => false,
		'choisir_quantite'    => false,
		'quantite_min'        => 1,
		'quantite_max'        => null,
		'quantite_seuil'      => 10,
		'quantite_saisie'     => 'auto',
		'submit_texte'        => _T('paniers:action_ajouter'),
		'submit_title'        => _T('paniers:action_ajouter_objet', ['objet' => generer_info_entite($id_objet, $objet, 'titre')]),
		'submit_class'        => '',
	];
	$options_merge = array_merge($options_defaut, $options);
	$options_static[$hash] = $options_merge;

	return $options_merge;
}


/**
 * Déclaration des saisies du formulaire de remplissage du panier
 *
 * @param string $objet
 *     Type d'objet.
 * @param int|string $id_objet
 *     Identifiant de l'objet.
 * @param array $options
 *     Cf. formulaires_remplir_panier_options_dist().
 * @param string $retour
 *     URL de redirection après le traitement
 * @return array
 *     Liste des saisies
 */
function formulaires_remplir_panier_saisies_dist(
	$objet,
	$id_objet,
	$options = [],
	$retour = ''
) {
	$saisies = [];
	$opts = formulaires_remplir_panier_options_dist($objet, $id_objet, $options, $retour);

	// Quantité
	if (!empty($opts['choisir_quantite'])) {
		$quantite_max    = ($opts['quantite_max'] ? intval($opts['quantite_max']) : null);
		$quantite_min    = intval($opts['quantite_min']);
		$quantite_seuil  = intval($opts['quantite_seuil']);
		$quantite_saisie = $opts['quantite_saisie'];
		// Type de saisie : soit un input number, soit un select.
		// Si auto, on bascule de select à input au delà d'un seuil.
		if (
			$quantite_saisie === 'auto'
		) {
			$quantite_saisie = (($quantite_max > 0 and $quantite_max < $quantite_seuil) ? 'selection' : 'input');
		}
		// Pas de select sans max.
		if (
			$quantite_saisie === 'selection'
			and !$quantite_max > 0
		) {
			$quantite_saisie = 'input';
		}
		// En commun aux 2 types de saisies
		$saisie_quantite = [
			'saisie' => $quantite_saisie,
			'options' => [
				'nom' => 'quantite',
				'obligatoire' => '',
				'label' => _T('paniers:champ_quantite_label'),
				'defaut' => $quantite_min,
				'id' => 'quantite_' . $objet . '' . $id_objet,
			],
			'verifier' => [
				'type' => 'entier',
			],
		];
		// Si saisie input
		if ($quantite_saisie === 'input') {
			$saisie_quantite = array_merge_recursive($saisie_quantite, [
				'options' => [
					'type' => 'number',
					'min'  => $quantite_min,
					'class' => 'quantite-nombre',
					'step' => 1,
				],
			]);
		// Si saisie select
		} else {
			$data = [];
			foreach (range($quantite_min, $quantite_max) as $val) {
				$data[$val] = $val;
			}
			$saisie_quantite = array_merge_recursive($saisie_quantite, [
				'options' => [
					'cacher_option_intro' => 'oui',
					'data' => $data,
				],
			]);
		}
		// Min et max si nécessaire
		if ($quantite_max) {
			$saisie_quantite['verifier']['options']['max'] = $quantite_max;
			$saisie_quantite['options']['max'] = $quantite_max;
		}
		if ($quantite_min) {
			$saisie_quantite['verifier']['options']['min'] = $quantite_min;
			$saisie_quantite['options']['min'] = $quantite_min;
		}
		// Done !
		$saisies[] = $saisie_quantite;
	}

	return $saisies;
}


/**
 * Identifier le formulaire afin de permettre de l'utiliser plusieurs fois sur une même page.
 *
 * @param string $objet
 *     Type d'objet à ajouter au panier.
 * @param int|string $id_objet
 *     Identifiant de l'objet à ajouter au panier.
 * @param array $options
 *     Cf. formulaires_remplir_panier_options_dist().
 * @param string $retour
 *     URL de redirection après le traitement
 * @return string
 *     Hash du formulaire
 */
function formulaires_remplir_panier_identifier_dist(
	$objet,
	$id_objet,
	$options = [],
	$retour = ''
) {
	return serialize([$objet, intval($id_objet)]);
}


/**
 * Chargement du formulaire de remplissage d'un panier
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @param string $objet
 *     Type d'objet à ajouter au panier.
 * @param int|string $id_objet
 *     Identifiant de l'objet à ajouter au panier.
 * @param array $options
 *     Cf. formulaires_remplir_panier_options_dist().
 * @param string $retour
 *     URL de redirection après le traitement
 * @return array
 *     Environnement du formulaire
 */
function formulaires_remplir_panier_charger_dist(
	$objet,
	$id_objet,
	$options = [],
	$retour = ''
) {

	$opts = formulaires_remplir_panier_options_dist($objet, $id_objet, $options, $retour);
	$valeurs = [
		'objet'         => $objet,
		'id_objet'      => $id_objet,
		'message_ok_js' => null,
	];

	// On ajoute les options à toute fin utile
	$valeurs = array_merge($valeurs, $opts);

	return $valeurs;
}

/**
 * Vérifications du formulaire de remplissage d'un panier
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @param string $objet
 *     Type d'objet à ajouter au panier.
 * @param int|string $id_objet
 *     Identifiant de l'objet à ajouter au panier.
 * @param array $options
 *     Cf. formulaires_remplir_panier_options_dist().
 * @param string $retour
 *     URL de redirection après le traitement
 * @return array
 *     Tableau des erreurs
 */
function formulaires_remplir_panier_verifier_dist(
	$objet,
	$id_objet,
	$options = [],
	$retour = ''
) {
	$erreurs = [];
	$opts = formulaires_remplir_panier_options_dist($objet, $id_objet, $options, $retour);

	// Quantité min
	if (
		$quantite = intval(_request('quantite'))
		and !empty($opts['quantite_min'])
		and $quantite_min = intval($opts['quantite_min'])
		and $quantite < $quantite_min
	) {
		$erreurs['quantite'] = _T('paniers:erreur_remplir_panier_quantite_min', ['min' => $quantite_min]);
	}

	// Quantité max
	if (
		$quantite = intval(_request('quantite'))
		and !empty($opts['quantite_max'])
		and $quantite_max = intval($opts['quantite_max'])
		and $quantite > $quantite_max
	) {
		$erreurs['quantite'] = _T('paniers:erreur_remplir_panier_quantite_max', ['max' => $quantite_max]);
	}

	return $erreurs;
}

/**
 * Traitement du formulaire de remplissage d'un panier
 *
 * Traiter les champs postés
 *
 * @uses action_remplir_panier_dist()
 *
 * @param string $objet
 *     Type d'objet à ajouter au panier.
 * @param int|string $id_objet
 *     Identifiant de l'objet à ajouter au panier.
 * @param array $options
 *     Cf. formulaires_remplir_panier_options_dist().
 * @param string $retour
 *     URL de redirection après le traitement
 * @return array
 *     Retours des traitements
 */
function formulaires_remplir_panier_traiter_dist(
	$objet,
	$id_objet,
	$options = [],
	$retour = ''
) {

	if (!empty($retour)) {
		refuser_traiter_formulaire_ajax();
	}

	$res = [
		'editable' => true,
	];
	$opts = formulaires_remplir_panier_options_dist($objet, $id_objet, $options, $retour);
	$message_ok = '';

	// Remplissons le panier…
	// Nb : l'action fait appel au pipeline 'remplir_panier'
	$quantite = (_request('quantite') ? intval(_request('quantite')) : $opts['quantite_min']);
	$remplir_panier = charger_fonction('remplir_panier', 'action');
	$data = $remplir_panier("$objet-$id_objet-$quantite");

	// Message de retour si demandé
	if (!empty($opts['afficher_message_ok'])) {
		$message_ok = singulier_ou_pluriel($quantite, 'paniers:info_1_objet_remplir_panier', 'paniers:info_nb_objets_remplir_panier');
	}

	// JS dans le message de retour
	$data = json_encode($data); // objet, id_objet, id_panier, quantite, negatif
	// Déclencher un évènement
	$js_event = "
		var data = $data;
		$( \"body\" ).trigger( \"remplir_panier\", data );
	";
	// Recharger un bloc ajax (optionnel pour rajouter des blocs, mais toujours "minipanier")
	// Nb : dans l'env du bloc rechargé, on passe les paramètres dans la clé `remplir_panier` pour éviter les conflits
	$js_ajaxreload = "
		var args = {};
		$.each(data, function( key, value ) {
			args['remplir_panier[' + key + ']'] = value;
		});
	";
	if (!empty($opts['ajaxreload'])) {
		$ajaxbloc = $opts['ajaxreload'];
		$js_ajaxreload .= "
			var ajaxbloc = '$ajaxbloc';
			if ($(ajaxbloc).length > 0) {
				$( ajaxbloc ).ajaxReload( {args: args} );
			} else {
				ajaxReload(ajaxbloc, {args: args} );
			}
		";
	}
	$js_ajaxreload .= "
		ajaxReload('minipanier', {args: args} );
	";
	// Emballons tout ça
	$js = "
		<script type=\"text/javascript\">
			;jQuery(function($) {
				$(function() {
					$js_event
					$js_ajaxreload
				});
			});
		</script>
	";
	// Indiquer si le message ne contient que du JS
	if ($js and !$message_ok) {
		set_request('message_ok_js', true);
	}
	$message_ok .= $js;

	// Message de retour
	if ($message_ok) {
		$res['message_ok'] = $message_ok;
	}

	// Redirection
	if ($retour) {
		$res['redirect'] = $retour;
	}

	return $res;
}
